import {FC} from "react";
import {Box, Code, Divider, Flex, Heading, Link, List, ListItem, Text} from "@chakra-ui/core";
import {GetServerSideProps, InferGetServerSidePropsType} from "next";
import Error from "next/error";
import Markdown, {MarkdownOptions} from "markdown-to-jsx";
import {humanize as humanizeString} from "@alduino/humanizer/string";
import humanizeDateTime from "@alduino/humanizer/dist/dateTime";
import DefaultDateTimeHumanizeStrategy from "@alduino/humanizer/dist/dateTime/DefaultDateTimeHumanizeStrategy";
import {ClassNames} from "@emotion/core";
import {packg} from "../../../lib/db/repositories";
import getPackageVisibility from "../../../lib/permissions/getPackageVisibility";
import {useAsyncEffect} from "@react-hook/async";

const languageMap = {
    js: "javascript",
    ts: "typescript"
};

const CodeDisplay: FC<{className: string}> = props => {
    const classes = props.className?.split(" ") || [];
    let language = classes.find(it => it.startsWith("lang-")).substring("lang-".length);
    if (language in languageMap) language = languageMap[language as keyof typeof languageMap];
    language = language.replace(/[^a-z0-9]/g, "");

    const {status: baseStatus, value: baseValue} = useAsyncEffect(() => Promise.all([
        import("react-syntax-highlighter/dist/esm/light"),
        import("react-syntax-highlighter/dist/esm/styles/hljs/mono-blue")
    ]), []);

    const {status: languageStatus, value: languageValue} = useAsyncEffect(() =>
        import("react-syntax-highlighter/dist/esm/languages/hljs/" + language),
        [language]
    );

    if (classes.some(it => it.startsWith("lang-")) && baseStatus === "success" && languageStatus !== "loading") {
        const [RSH, githubGist] = baseValue;

        if (languageStatus === "success") RSH.default.registerLanguage(language, languageValue.default);

        return (
            <ClassNames>
                {({cx}) => (
                    <RSH.default
                        style={githubGist.default}
                        css={theme => ({margin: ".5em 0", borderRadius: theme.radii.sm})}
                        {...props}
                        className={cx(props.className, "lang-" + language)}
                    >
                        {props.children}
                    </RSH.default>
                )}
            </ClassNames>
        );
    }
    return <Code {...props} />;
};

const markdownOptions: MarkdownOptions = {
    overrides: {
        h1: {
            component: Heading,
            props: {
                as: "h1",
                size: "lg"
            }
        },
        h2: {
            component: Heading,
            props: {
                as: "h2",
                size: "lg"
            }
        },
        h3: {
            component: Heading,
            props: {
                as: "h3",
                size: "md"
            }
        },
        h4: {
            component: Heading,
            props: {
                as: "h4",
                size: "sm"
            }
        },
        h5: {
            component: Heading,
            props: {
                as: "h5",
                size: "sm"
            }
        },
        h6: {
            component: Heading,
            props: {
                as: "h6",
                size: "sm"
            }
        },
        p: {
            component: Text,
            props: {
                mt: 4,
                mb: 4
            }
        },
        a: {
            component: Link,
            props: {
                color: "blue.500"
            }
        },
        code: {
            component: CodeDisplay
        },
        ul: {
            component: List,
            props: {
                styleType: "disc"
            }
        },
        ol: {
            component: List,
            props: {
                as: "ol",
                styleType: "decimal"
            }
        },
        li: {
            component: ListItem
        }
    }
};

export default function Package(props: InferGetServerSidePropsType<typeof getServerSideProps>) {
    if (props.notFound) {
        return (
            <Error statusCode={404} />
        );
    }

    return (
        <Box>
            <Flex as="header" p={4} borderWidth={1} rounded="md" align="center">
                <Heading size="lg" flexGrow={1}>{props.packageName}</Heading>
                <Text>{props.package.latestVersion.versionName}</Text>
                <Divider orientation="vertical" height={4} />
                <Text>{humanizeString(props.visibility)}</Text>
                <Divider orientation="vertical" height={4} />
                <Text>Published {humanizeDateTime(new Date(props.package.latestVersion.publishDate), void 0, new DefaultDateTimeHumanizeStrategy())}</Text>
            </Flex>

            <Box mt={8} as="article">
                <Markdown options={markdownOptions}>{props.package.latestVersion.readme}</Markdown>
            </Box>
        </Box>
    );
}

export const getServerSideProps: GetServerSideProps = async ctx => {
    const packageName = (ctx.params.name as string[]).join("/");
    const feedName = ctx.params.feed as string;

    const pkg = await packg.then(pk => pk.findOne({
        where: {
            feed: {
                id: feedName
            },
            name: packageName
        },
        relations: ["latestVersion", "versions", "feed"]
    })).catch(() => null);

    const visibility = pkg ? await getPackageVisibility(ctx, pkg) : null;

    if (!pkg || !visibility) ctx.res.statusCode = 404;

    return {
        props: {
            notFound: ctx.res.statusCode === 404,
            packageName,
            visibility: pkg ? await getPackageVisibility(ctx, pkg) : null,
            package: JSON.parse(JSON.stringify(pkg) || "{}")
        }
    }
};
