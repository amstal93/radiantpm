import {NextApiRequest, NextApiResponse} from "next";
import {feed} from "../../../../lib/db/repositories";
import {Feed, FeedFormat, Package} from "../../../../lib/db/orm";
import {filterAsync} from "../../../../lib/async-array";
import canViewPackage from "../../../../lib/permissions/canViewPackage";
import setupServices from "../../../../lib/setupServices";
import removeProperty from "../../../../lib/removeProperty";

interface ForwardedHeader {
    by: string;
    for: string;
    host: string;
    proto: "http" | "https";
}

function parseForwardedHeader(source: string): Partial<ForwardedHeader> {
    const parts = source.split(";");

    return Object.fromEntries(
        parts
            .map(part => part.split("="))
            .map(([k, v]) => [k.toLowerCase(), v])
    );
}

function constructForwarded(req: NextApiRequest): ForwardedHeader {
    return {
        by: "none",
        for: req.connection.remoteAddress,
        host: req.headers.host,
        proto: "http"
    }
}

async function handleNuGetPackage(req: NextApiRequest, res: NextApiResponse, feedItem: Feed) {
    const baseUrlParts = req.headers.forwarded ?
        {...constructForwarded(req), ...parseForwardedHeader(req.headers.forwarded)} :
        constructForwarded(req);

    // assumes default port
    const baseUrl = `${baseUrlParts.proto}://${baseUrlParts.host}/api/feeds/${feedItem.id}`;

    res.status(200).json({
        ...feedItem,
        version: "3.0.0",
        resources: [
            {
                "@id": baseUrl + "/-/nuget/package-base-address",
                "@type": "PackageBaseAddress/3.0.0",
                comment: "Redirects to .nupkg file path. Authentication required for access."
            }
        ]
    });
}

setupServices();

export default async (req: NextApiRequest, res: NextApiResponse) => {
    const thisFeed = await feed.then(fd => fd.findOne({where: {id: req.query.feed}, relations: ["packages"]}));
    if (thisFeed == null) {
        res.status(404).json({
            error: "Not found"
        });
        return;
    }

    const feedWithHiddenPackages: typeof thisFeed = {
        ...thisFeed,
        packages: await filterAsync(
            thisFeed.packages.map(pkg => ({...pkg, feed: thisFeed})),
                el => canViewPackage({req, res}, el))
            .then(res => res.map(r => removeProperty(r, "feed"))) as Package[]
    };

    switch (thisFeed.format) {
        case FeedFormat.NuGet:
            await handleNuGetPackage(req, res, feedWithHiddenPackages);
            break;
        default:
            res.status(200).json(feedWithHiddenPackages);
    }
};
