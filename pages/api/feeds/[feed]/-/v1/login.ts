import {NextApiRequest, NextApiResponse} from "next";
import {randomBytes} from "crypto";

const token = randomBytes(16).toString("hex");

export default async (req: NextApiRequest, res: NextApiResponse) => {
    res.status(200).json({
        doneUrl: `http://${req.headers.host}/api/login-flow/wait?token=${token}`,
        loginUrl: `http://${req.headers.host}/login?redirect=${encodeURIComponent(`/api/login-flow/end?token=${token}`)}`
    });
};
