import {NextApiRequest, NextApiResponse} from "next";
import {packg, version} from "../../../../../../../../lib/db/repositories";
import CustomFindOperator from "../../../../../../../../lib/db/CustomFindOperator";
import canViewPackage from "../../../../../../../../lib/permissions/canViewPackage";
import setupServices from "../../../../../../../../lib/setupServices";

setupServices();

export default async function (req: NextApiRequest, res: NextApiResponse) {
    const pkg = await packg.then(pkg => pkg.findOne({
        where: {
            name: new CustomFindOperator((alias, [pkgName]) => `LOWER(${alias}) = ${pkgName}`,
                [(req.query.package as string).toLowerCase()])
        },
        relations: ["feed"]
    }));

    if (!pkg || !await canViewPackage({req, res}, pkg)) {
        res.status(404).json({
            error: "Not found"
        });
        return;
    }

    const versions = await version.then(v => v.find({
        where: {
            package: pkg
        }
    }));

    res.status(200).json({
        versions: versions.map(v => v.versionName)
    });
}
