import {NextApiRequest, NextApiResponse} from "next";
import {parse as parseContentType} from "content-type";
import {IncomingHttpHeaders} from "http";
import {feed} from "../../../lib/db/repositories";
import {FeedFormat} from "../../../lib/db/orm";
import {inject} from "../../../lib/dependency-injection";
import GitlabApi, {AccessLevel} from "../../../lib/GitlabApi";
import setupServices from "../../../lib/setupServices";
import settings from "../../../lib/settings";
import canViewPackage from "../../../lib/permissions/canViewPackage";
import {filterAsync, mapAsync} from "../../../lib/async-array";

interface RecursiveDictionary<V> {
    [key: string]: V | RecursiveDictionary<V>;
}

function parseFormData(body: string, contentType: string): RecursiveDictionary<string> {
    const {type, parameters: {boundary}} = parseContentType(contentType);
    if (!type.startsWith("multipart/")) throw new Error("Invalid content type");

    const parts = body.split(`--${boundary}`);
    const lastIndex = parts.indexOf("--");
    const realParts = parts.slice(1, lastIndex);

    return Object.fromEntries(realParts.map(part => {
        const headers: IncomingHttpHeaders = Object.fromEntries(part.substring(2, part.indexOf("\r\n\r\n"))
            .split("\r\n")
            .map(header => [
                header.substring(0, header.indexOf(":")).trim().toLowerCase(),
                header.substring(header.indexOf(":") + 1).trim()
            ])
        );

        const contentDisposition = parseContentType("fool-content-type/" + headers["content-disposition"]);
        if (contentDisposition.type !== "fool-content-type/form-data") throw new Error("Only form-data is supported");
        if (!contentDisposition.parameters.name) throw new Error("No key specified");

        const {parameters: {name}} = contentDisposition;

        const body = part.substring(part.indexOf("\r\n\r\n") + 4).trim();

        if (headers["content-type"] && parseContentType(headers["content-type"]).type.startsWith("multipart/")) {
            return [name, parseFormData(body, headers["content-type"])];
        }

        return [name, body];
    }));
}

setupServices();

export default async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "GET") {
        const items = await feed.then(fd => fd.find({relations: ["packages"]}));
        const visibleItems = await mapAsync(items, async fd => ({
            ...fd,
            packages: await filterAsync(fd.packages, pkg => canViewPackage({req, res}, {...pkg, feed: fd}))
        }));
        res.status(200).json(visibleItems);
    } else if (req.method === "POST") {
        const {feedName = "", groupName = "", feedType = "", description = ""} =
            parseFormData(req.body, req.headers["content-type"]) as {[key: string]: string};

        const feedTypeNo = parseInt(feedType);

        const api: GitlabApi = inject({req, res}, GitlabApi);

        if (feedName.length === 0) return res.status(400).json({
            error: "Feed name is required",
            for: "feedName"
        });

        if (!/^[a-z0-9_-]+$/.test(feedName)) return res.status(400).json({
            error: "Invalid feed name",
            for: "feedName"
        });

        if (!FeedFormat[feedTypeNo]) return res.status(400).json({
            error: "Invalid feed type",
            for: "feedType"
        });

        if (settings.allowedGroups.length > 0 && !settings.allowedGroups.includes(groupName)) {
            return res.status(400).json({
                error: "Group not allowed",
                for: "groupName"
            });
        }

        const groups = await api?.getGroups({min_access_level: AccessLevel.Owner}).catch(() => []);

        if (groupName &&!groups.some(it => it.full_name === groupName)) {
            return res.status(400).json({
                error: "Not group owner",
                for: "groupName"
            });
        }

        if (await feed.then(fd => fd.count({
            where: {
                id: feedName
            }
        })) > 0) {
            return res.status(409).json({
                error: "Feed already exists",
                for: "feedName"
            });
        }

        try {
            await feed.then(fd => fd.insert({
                id: feedName,
                group: groupName,
                format: feedTypeNo,
                description
            }));
        } catch (err) {
            if (err.message.startsWith("duplicate key value")) return res.status(409).json({
                error: "Feed already exists",
                for: "feedName"
            });

            return res.status(500).json({
                error: "An error occurred"
            });
        }

        res.setHeader("Location", `/api/feeds/${feedName}`);
        res.status(201).json({
            success: true
        });
    }
};
