export async function filterAsync<T>(arr: T[], predicate: (el: T, idx: number) => Promise<boolean>) {
    const results = await Promise.all(arr.map(predicate));
    return arr.filter((_, i) => results[i]);
}

export function mapAsync<T, V>(arr: T[], map: (el: T, idx: number) => Promise<V>) {
    return Promise.all(arr.map(map));
}
