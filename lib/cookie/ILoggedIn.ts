export default interface ILoggedIn {
    accessToken: string;
}
