export default function removeProperty<T, P extends string | number | symbol>(obj: T, property: P): Omit<T, P> {
    return Object.fromEntries(Object.entries(obj).filter(([key]) => key !== property)) as Omit<T, P>;
}
