import {register, ServiceLifetime} from "./dependency-injection";
import GitlabApi from "./GitlabApi";
import {parseCookies} from "nookies";
import isLoggedIn from "./isLoggedIn";

export default function setupServices() {
    // Register DI services here
    register(GitlabApi, ServiceLifetime.Transient, ctx => {
        const cookies = parseCookies(ctx);
        if (!isLoggedIn(cookies)) return null;
        return new GitlabApi(cookies.accessToken);
    });
}
